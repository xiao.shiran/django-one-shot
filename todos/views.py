from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list" : list,
    }
    return render(request, "todos/todolist.html", context)

def todo_list_detail(request, id):
    list_detail = TodoList.objects.get(id=id)
    context = {
        "list_object" : list_detail,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id = list.id)
    else:
        form = TodoForm()

    context = {
        "form" : form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    list_detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list_detail)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id = list.id)
    else:
        form = TodoForm(instance=list_detail)

    context = {
        "list_detial" : list_detail,
        "form" : form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id = item.list.id)
    else:
        form = ItemForm()

    context = {
        "form" : form,
    }
    return render(request, "todos/add.html", context)

def todo_item_update(request, id):
    item_detail = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item_detail)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id = item.list.id)
    else:
        form = ItemForm(instance=item_detail)

    context = {
        "item_detial" : item_detail,
        "form" : form,
    }
    return render(request, "todos/itemedit.html", context)
